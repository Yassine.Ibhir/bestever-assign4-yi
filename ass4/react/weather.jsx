
// Yassine Ibhir

'use strict'

/**
 * Class component.
 *  Creates, mounts and renders the component.
 */
class Weather extends React.Component{

    /**
     *  Initiliazes the weather component
     * and its state
     * @param {city name} props 
     */
    constructor(props){
        super(props);
        this.state = {
            info :  null
        };
    }

 
    /**
     * Fetch the data and change the state
     */
componentDidMount(){

    const key = "b357446ecdda71bfc23e8d3fd9a9ac72";
    
    let obj = {
       
        q: this.props.city,
       
        appid: key
   }

   const params = new URLSearchParams(obj);
    
   let url = "https://api.openweathermap.org/data/2.5/weather?"+params.toString();
   

   fetch(url)
   .then(response =>{
       if(response.ok){

         return  response.json();
           
       }

       throw new Error("Weather is not available at this time");
       
   })
   .then(data => this.setState({info : data}))
   .catch( (error) => this.setState({info : error.toString()}));
   
    }

    /**
     * Create and display the component based on the state
     */

    render(){


        if(!(typeof this.state.info === 'string') && this.state.info){

            let imageUrl = "http://openweathermap.org/img/wn/"+this.state.info.weather[0].icon+"@2x.png";

            let degreeCelcius = (this.state.info.main.temp - 273.15).toFixed(2) ;
            return (
                <article>
                    
                    <p> {degreeCelcius} °C {this.state.info.weather[0].description}</p>
                    <img src = {imageUrl}/>
                    <button onClick = {() => this.componentDidMount()}>refresh</button>
    
                </article>
            ); 

        }
    
    else{
        
        return(
         
                <article>
            
                    <p>{this.state.info}</p>
                    <button onClick = {() => this.componentDidMount()}>refresh</button>
    
                </article>

        );
    }
}
}

let container = document.querySelector('#container');

// render the component inside container element.
ReactDOM.render(<Weather city='montreal'/>, 
    container);
